var fs = require('fs');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var marked = require('marked');
var hl = require('highlight.js');

marked.setOptions({
  highlight: function (code, lang) {
    var l = hl.getLanguage(lang);

    if (l) {
      return hl.highlight(lang, code).value;
    } else {
      return hl.highlightAuto(code).value;
    }
  }
});

var styles = fs.readdirSync(__dirname + '/public/styles');

app.set('view engine', 'jade');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.render('index', {styles: styles, sel: 'default.css'});
});

app.post('/', function (req, res) {
  res.render('document', {style: req.body.style, code: marked(req.body.md)});
});

var server = app.listen(3000, function () {
  console.log('started listening on port %d', server.address().port);
});